package com.legacy.pagamos.item;

import com.legacy.pagamos.block.ColdFireBlock;
import com.legacy.pagamos.block.PagamosPortalBlock;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.FlintAndSteelItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

public class FlintAndLapisItem extends FlintAndSteelItem
{
	public FlintAndLapisItem(Properties builder)
	{
		super(builder);
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context)
	{
		PlayerEntity playerentity = context.getPlayer();
		IWorld iworld = context.getWorld();
		BlockPos blockpos = context.getPos();
		BlockPos face = blockpos.offset(context.getFace());

		if (((PagamosPortalBlock) PagamosBlocks.pagamos_portal).trySpawnPortal(iworld, face))
		{
			iworld.playSound(playerentity, blockpos, PagamosSounds.portal_create, SoundCategory.BLOCKS, 1.0F, 1.0F);
			iworld.playSound(playerentity, blockpos, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0F, random.nextFloat() * 0.4F + 0.8F);
			return ActionResultType.SUCCESS;
		}
		else if (canSetFire(iworld.getBlockState(face), iworld, face))
		{
			iworld.playSound(playerentity, face, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0F, random.nextFloat() * 0.4F + 0.8F);
			BlockState blockstate1 = ((ColdFireBlock) PagamosBlocks.blue_fire).getStateForPlacement(iworld, face);
			iworld.setBlockState(face, blockstate1, 11);
			ItemStack itemstack = context.getItem();
			if (playerentity instanceof ServerPlayerEntity)
			{
				CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) playerentity, face, itemstack);
				itemstack.damageItem(1, playerentity, (p_219998_1_) ->
				{
					p_219998_1_.sendBreakAnimation(context.getHand());
				});
			}

			return ActionResultType.SUCCESS;
		}
		else
		{
			return super.onItemUse(context);
		}
	}
}
