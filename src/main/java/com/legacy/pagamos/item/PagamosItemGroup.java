package com.legacy.pagamos.item;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class PagamosItemGroup extends ItemGroup
{
	public static final PagamosItemGroup PAGAMOS = new PagamosItemGroup("pagamos")
	{
		@Override
		@OnlyIn(Dist.CLIENT)
		public ItemStack createIcon()
		{
			return new ItemStack(Blocks.BLUE_ICE);
		}
	};

	public PagamosItemGroup(String label)
	{
		super("pagamos." + label);
	}
}