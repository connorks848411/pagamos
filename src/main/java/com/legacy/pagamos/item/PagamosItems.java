package com.legacy.pagamos.item;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.entity.PagamosEntityTypes;

import net.minecraft.item.Item;
import net.minecraft.item.ItemTier;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.item.SwordItem;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class PagamosItems
{
	private static IForgeRegistry<Item> iItemRegistry;

	public static Item crystal_dust, creeper_dust, ice_shard, amethyst, sapphire, phoenix_feather, ice_rod,
			flint_and_lapis, ice_brick, crystal_gem;

	public static Item amethyst_sword, amethyst_pickaxe, amethyst_shovel, amethyst_spear;

	public static Item crystal_sword, crystal_pickaxe, crystal_axe, crystal_shovel, ice_spear;

	public static Item yeti_spawn_egg, phoenix_spawn_egg, ice_creeper_spawn_egg;
	/*public static ToolMaterial AMETHYST = EnumHelper.addToolMaterial("AMETHYST", 2, 250, 12.8F, 8.0F, 14).setRepairItem(new ItemStack(IceItems.amethyst));
	
	public static ToolMaterial ICE = EnumHelper.addToolMaterial("ICE", 1, 131, 4.0F, 1.0F, 5).setRepairItem(new ItemStack(IceItems.ice_shard));*/

	public static void init(RegistryEvent.Register<Item> event)
	{
		iItemRegistry = event.getRegistry();

		yeti_spawn_egg = register("yeti_spawn_egg", new SpawnEggItem(PagamosEntityTypes.YETI, 0xffffff, 0xc1bc9f, new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		phoenix_spawn_egg = register("phoenix_spawn_egg", new SpawnEggItem(PagamosEntityTypes.PHOENIX, 0x4083f1, 0xb4cdf7, new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		ice_creeper_spawn_egg = register("ice_creeper_spawn_egg", new SpawnEggItem(PagamosEntityTypes.ICE_CREEPER, 0xb5dee4, 0, new Item.Properties().group(PagamosItemGroup.PAGAMOS)));

		flint_and_lapis = register("flint_and_lapis", new FlintAndLapisItem((new Item.Properties().group(PagamosItemGroup.PAGAMOS)).maxDamage(64)));
		crystal_dust = register("crystal_dust", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		creeper_dust = register("creeper_dust", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		ice_brick = register("ice_brick", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		ice_shard = register("ice_shard", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		amethyst = register("amethyst", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		sapphire = register("sapphire", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		crystal_gem = register("crystal_gem", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		phoenix_feather = register("phoenix_feather", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));
		ice_rod = register("ice_rod", new Item(new Item.Properties().group(PagamosItemGroup.PAGAMOS)));

		amethyst_sword = register("amethyst_sword", new SwordItem(ItemTier.IRON, 3, -2.4F, (new Item.Properties()).group(PagamosItemGroup.PAGAMOS)));
		amethyst_pickaxe = register("amethyst_pickaxe", new PickaxeItem(ItemTier.IRON, 1, -2.8F, (new Item.Properties()).group(PagamosItemGroup.PAGAMOS)));
		amethyst_shovel = register("amethyst_shovel", new ShovelItem(ItemTier.IRON, 1.5F, -3.0F, (new Item.Properties()).group(PagamosItemGroup.PAGAMOS)));
		// amethyst_spear = register("amethyst_spear", new ItemAmethystSpear());

		crystal_sword = register("crystal_sword", new SwordItem(ItemTier.STONE, 3, -2.4F, (new Item.Properties()).group(PagamosItemGroup.PAGAMOS)));
		crystal_pickaxe = register("crystal_pickaxe", new PickaxeItem(ItemTier.STONE, 1, -2.8F, (new Item.Properties()).group(PagamosItemGroup.PAGAMOS)));
		crystal_shovel = register("crystal_shovel", new ShovelItem(ItemTier.STONE, 1.5F, -3.0F, (new Item.Properties()).group(PagamosItemGroup.PAGAMOS)));
		// crystal_axe = register("crystal_axe", new ItemAxe());
	}

	private static Item register(String unlocalizedName, Item item)
	{
		item.setRegistryName(PagamosMod.locate(unlocalizedName));
		iItemRegistry.register(item);
		return item;
	}
}