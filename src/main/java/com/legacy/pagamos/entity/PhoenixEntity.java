package com.legacy.pagamos.entity;

import java.util.Random;

import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.monster.GhastEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class PhoenixEntity extends GhastEntity
{
	public boolean field_753_a;
	public float field_752_b, destPos, field_757_d, field_756_e, field_755_h;

	public PhoenixEntity(EntityType<? extends PhoenixEntity> type, World worldIn)
	{
		super(type, worldIn);

		field_753_a = false;
		field_752_b = 0.0F;
		destPos = 0.0F;
		field_755_h = 1.0F;
	}

	@Override
	public void livingTick()
	{
		super.livingTick();

		if (world.isRemote)
		{
			field_756_e = field_752_b;
			field_757_d = destPos;
			destPos = (float) ((double) destPos + (double) (onGround ? -1 : 4) * 0.29999999999999999D);
			if (destPos < 0.0F)
			{
				destPos = 0.0F;
			}
			if (destPos > 1.0F)
			{
				destPos = 1.0F;
			}
			if (!onGround && field_755_h < 1.0F)
			{
				field_755_h = 1.0F;
			}
			field_755_h = (float) ((double) field_755_h * 0.90000000000000002D);
			field_752_b += field_755_h * 2.0F;
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return PagamosSounds.phoenix_idle;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return PagamosSounds.phoenix_idle;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return PagamosSounds.phoenix_death;
	}

	public static boolean canSpawn(EntityType<? extends PhoenixEntity> typeIn, IWorld worldIn, SpawnReason reason, BlockPos pos, Random randomIn)
	{
		return worldIn.getRandom().nextInt(1000) == 0 && MobEntity.canSpawnOn(typeIn, worldIn, reason, pos, randomIn);
	}
}
