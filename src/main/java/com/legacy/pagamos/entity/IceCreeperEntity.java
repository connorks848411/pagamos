package com.legacy.pagamos.entity;

import java.util.Random;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class IceCreeperEntity extends CreeperEntity
{
	public IceCreeperEntity(EntityType<? extends IceCreeperEntity> type, World worldIn)
	{
		super(type, worldIn);
	}

	public static boolean canSpawn(EntityType<? extends IceCreeperEntity> typeIn, IWorld worldIn, SpawnReason reason, BlockPos pos, Random randomIn)
	{
		return worldIn.getRandom().nextInt(80) == 0 && MobEntity.canSpawnOn(typeIn, worldIn, reason, pos, randomIn);
	}
}
