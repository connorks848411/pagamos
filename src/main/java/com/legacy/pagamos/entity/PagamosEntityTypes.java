package com.legacy.pagamos.entity;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.PagamosRegistry;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(PagamosMod.MODID)
public class PagamosEntityTypes
{

	public static final EntityType<YetiEntity> YETI = buildEntity("yeti", EntityType.Builder.create(YetiEntity::new, EntityClassification.CREATURE).size(0.7F, 2.2F));

	public static final EntityType<PhoenixEntity> PHOENIX = buildEntity("phoenix", EntityType.Builder.create(PhoenixEntity::new, EntityClassification.MONSTER).size(2F, 2.5F).immuneToFire());
	public static final EntityType<IceCreeperEntity> ICE_CREEPER = buildEntity("ice_creeper", EntityType.Builder.create(IceCreeperEntity::new, EntityClassification.MONSTER).size(0.6F, 1.7F));

	public static void init(Register<EntityType<?>> event)
	{
		PagamosRegistry.register(event.getRegistry(), "yeti", YETI);
		PagamosRegistry.register(event.getRegistry(), "phoenix", PHOENIX);
		PagamosRegistry.register(event.getRegistry(), "ice_creeper", ICE_CREEPER);

		registerSpawnConditions();
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(PagamosMod.find(key));
	}

	private static void registerSpawnConditions()
	{
		EntitySpawnPlacementRegistry.register(PagamosEntityTypes.YETI, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, YetiEntity::canSpawn);
		EntitySpawnPlacementRegistry.register(PagamosEntityTypes.PHOENIX, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, PhoenixEntity::canSpawn);
		EntitySpawnPlacementRegistry.register(PagamosEntityTypes.ICE_CREEPER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, IceCreeperEntity::canSpawn);
	}
}
