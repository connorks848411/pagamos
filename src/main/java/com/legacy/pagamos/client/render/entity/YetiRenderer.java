package com.legacy.pagamos.client.render.entity;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.client.model.YetiModel;
import com.legacy.pagamos.entity.YetiEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class YetiRenderer extends MobRenderer<YetiEntity, YetiModel<YetiEntity>>
{
	private static final ResourceLocation TEXTURE = PagamosMod.locate("textures/entity/yeti.png");

	public YetiRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new YetiModel<>(), 0.5F);
	}

	@Override
	public ResourceLocation getEntityTexture(YetiEntity entity)
	{
		return TEXTURE;
	}
}