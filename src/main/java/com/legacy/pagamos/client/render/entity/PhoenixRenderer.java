package com.legacy.pagamos.client.render.entity;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.client.model.PhoenixModel;
import com.legacy.pagamos.entity.PhoenixEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PhoenixRenderer extends MobRenderer<PhoenixEntity, PhoenixModel<PhoenixEntity>>
{
	private static final ResourceLocation TEXTURE = PagamosMod.locate("textures/entity/phoenix.png");

	public PhoenixRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn, new PhoenixModel<>(), 0.5F);
	}

	protected float getWingRotation(PhoenixEntity entityphoenix, float f)
	{
		float f1 = (entityphoenix.field_756_e + (entityphoenix.field_752_b - entityphoenix.field_756_e) * f) / 8F + 0.6F;
		float f2 = (entityphoenix.field_757_d + (entityphoenix.destPos - entityphoenix.field_757_d) * f) / 8F + 0.6F;
		return (MathHelper.sin(f1) + 1.0F) * f2;
	}

	@Override
	protected float handleRotationFloat(PhoenixEntity entityliving, float f)
	{
		return getWingRotation(entityliving, f);
	}

	@Override
	protected void preRenderCallback(PhoenixEntity entitylivingbaseIn, MatrixStack matrixStackIn, float partialTickTime)
	{
		super.preRenderCallback(entitylivingbaseIn, matrixStackIn, partialTickTime);
		matrixStackIn.scale(2.0F, 2.0F, 2.0F);
	}

	@Override
	public ResourceLocation getEntityTexture(PhoenixEntity entity)
	{
		return TEXTURE;
	}
}