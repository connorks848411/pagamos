package com.legacy.pagamos.client.render;

import com.legacy.pagamos.client.render.entity.IceCreeperRenderer;
import com.legacy.pagamos.client.render.entity.PhoenixRenderer;
import com.legacy.pagamos.client.render.entity.YetiRenderer;
import com.legacy.pagamos.entity.PagamosEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class PagamosEntityRendering
{

	public static void init()
	{
		register(PagamosEntityTypes.YETI, YetiRenderer::new);
		register(PagamosEntityTypes.PHOENIX, PhoenixRenderer::new);
		register(PagamosEntityTypes.ICE_CREEPER, IceCreeperRenderer::new);
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}