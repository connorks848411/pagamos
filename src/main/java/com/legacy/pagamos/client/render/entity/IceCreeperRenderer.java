package com.legacy.pagamos.client.render.entity;

import com.legacy.pagamos.PagamosMod;

import net.minecraft.client.renderer.entity.CreeperRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class IceCreeperRenderer extends CreeperRenderer
{
	private static final ResourceLocation TEXTURE = PagamosMod.locate("textures/entity/ice_creeper.png");

	public IceCreeperRenderer(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
	}

	@Override
	public ResourceLocation getEntityTexture(CreeperEntity entity)
	{
		return TEXTURE;
	}
}