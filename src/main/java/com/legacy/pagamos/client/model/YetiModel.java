package com.legacy.pagamos.client.model;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class YetiModel<T extends Entity> extends SegmentedModel<T>
{

	public YetiModel()
	{
		hair = new ModelRenderer(this, 18, 0);
		hair.addBox(-3F, -10F, -3F, 6, 1, 6, 0.0F);
		hair.setRotationPoint(0.0F, -1F, 0.0F);
		hair.rotateAngleX = 0.0F;
		hair.rotateAngleY = 0.0F;
		hair.rotateAngleZ = 0.0F;
		hair.mirror = false;
		belly = new ModelRenderer(this, 2, 21);
		belly.addBox(-4.5F, 6F, -4F, 9, 7, 4, 0.0F);
		belly.setRotationPoint(0.0F, -3F, 0.0F);
		belly.rotateAngleX = 0.0F;
		belly.rotateAngleY = 0.0F;
		belly.rotateAngleZ = 0.0F;
		belly.mirror = false;
		lefttooth = new ModelRenderer(this, 26, 10);
		lefttooth.addBox(1.6F, -5F, -4.6F, 2, 3, 2, 0.0F);
		lefttooth.setRotationPoint(0.0F, -1F, 0.0F);
		lefttooth.rotateAngleX = 0.0F;
		lefttooth.rotateAngleY = 0.0F;
		lefttooth.rotateAngleZ = 0.0F;
		lefttooth.mirror = true;
		leftarm = new ModelRenderer(this, 48, 16);
		leftarm.addBox(-1F, -2F, -2F, 4, 12, 4, 0.0F);
		leftarm.setRotationPoint(5F, 0.0F, 0.0F);
		leftarm.rotateAngleX = 0.0F;
		leftarm.rotateAngleY = 0.0F;
		leftarm.rotateAngleZ = 0.0F;
		leftarm.mirror = false;
		rightleg = new ModelRenderer(this, 48, 16);
		rightleg.addBox(-2F, -1F, -2F, 4, 12, 4, 0.0F);
		rightleg.setRotationPoint(-2F, 10F, 0.0F);
		rightleg.rotateAngleX = 0.0F;
		rightleg.rotateAngleY = 0.0F;
		rightleg.rotateAngleZ = 0.0F;
		rightleg.mirror = false;
		body = new ModelRenderer(this, 0, 14);
		body.addBox(-4F, 1.0F, -3F, 8, 12, 6, 0.0F);
		body.setRotationPoint(0.0F, -2F, 0.0F);
		body.rotateAngleX = 0.0F;
		body.rotateAngleY = 0.0F;
		body.rotateAngleZ = 0.0F;
		body.mirror = false;
		head = new ModelRenderer(this, 36, 0);
		head.addBox(-3.5F, -9F, -3.5F, 7, 8, 7, 0.0F);
		head.setRotationPoint(0.0F, -1F, 0.0F);
		head.rotateAngleX = 0.0F;
		head.rotateAngleY = 0.0F;
		head.rotateAngleZ = 0.0F;
		head.mirror = false;
		rightarm = new ModelRenderer(this, 48, 16);
		rightarm.addBox(-3F, -2F, -2F, 4, 12, 4, 0.0F);
		rightarm.setRotationPoint(-5F, 0.0F, 0.0F);
		rightarm.rotateAngleX = 0.0F;
		rightarm.rotateAngleY = 0.0F;
		rightarm.rotateAngleZ = 0.0F;
		rightarm.mirror = false;
		leftfoot = new ModelRenderer(this, 0, 4);
		leftfoot.addBox(-2.1F, 11.1F, -6.1F, 4, 2, 8, 0.0F);
		leftfoot.setRotationPoint(2.0F, 11F, 0.0F);
		leftfoot.rotateAngleX = 0.0F;
		leftfoot.rotateAngleY = 0.0F;
		leftfoot.rotateAngleZ = 0.0F;
		leftfoot.mirror = false;
		rightshoulder = new ModelRenderer(this, 28, 22);
		rightshoulder.addBox(-4F, -3F, -2.5F, 5, 5, 5, 0.0F);
		rightshoulder.setRotationPoint(-5F, 0.0F, 0.0F);
		rightshoulder.rotateAngleX = 0.0F;
		rightshoulder.rotateAngleY = 0.0F;
		rightshoulder.rotateAngleZ = 0.0F;
		rightshoulder.mirror = false;
		Chin = new ModelRenderer(this, 28, 17);
		Chin.addBox(-4F, -4F, -4.5F, 8, 3, 2, 0.0F);
		Chin.setRotationPoint(0.0F, -1F, 0.0F);
		Chin.rotateAngleX = 0.0F;
		Chin.rotateAngleY = 0.0F;
		Chin.rotateAngleZ = 0.0F;
		Chin.mirror = false;
		righttooth = new ModelRenderer(this, 26, 10);
		righttooth.addBox(-3.6F, -5F, -4.6F, 2, 3, 2, 0.0F);
		righttooth.setRotationPoint(0.0F, -1F, 0.0F);
		righttooth.rotateAngleX = 0.0F;
		righttooth.rotateAngleY = 0.0F;
		righttooth.rotateAngleZ = 0.0F;
		righttooth.mirror = false;
		leftshoulder = new ModelRenderer(this, 28, 22);
		leftshoulder.addBox(-1F, -3F, -2.5F, 5, 5, 5, 0.0F);
		leftshoulder.setRotationPoint(5F, 0.0F, 0.0F);
		leftshoulder.rotateAngleX = 0.0F;
		leftshoulder.rotateAngleY = 0.0F;
		leftshoulder.rotateAngleZ = 0.0F;
		leftshoulder.mirror = true;
		leftleg = new ModelRenderer(this, 48, 16);
		leftleg.addBox(-2F, -1F, -2F, 4, 12, 4, 0.0F);
		leftleg.setRotationPoint(2.0F, 10F, 0.0F);
		leftleg.rotateAngleX = 0.0F;
		leftleg.rotateAngleY = 0.0F;
		leftleg.rotateAngleZ = 0.0F;
		leftleg.mirror = false;
		rightfoot = new ModelRenderer(this, 0, 4);
		rightfoot.addBox(-2.1F, 11.1F, -6.1F, 4, 2, 8, 0.0F);
		rightfoot.setRotationPoint(-2F, 11F, 0.0F);
		rightfoot.rotateAngleX = 0.0F;
		rightfoot.rotateAngleY = 0.0F;
		rightfoot.rotateAngleZ = 0.0F;
		rightfoot.mirror = true;
	}

	@Override
	public Iterable<ModelRenderer> getParts()
	{
		return ImmutableList.of(body, belly, head, hair, rightarm, leftarm, leftfoot, rightshoulder, Chin, righttooth, lefttooth, leftshoulder, leftleg, rightleg, rightfoot);
	}

	@Override
	public void setRotationAngles(Entity entity, float f, float f1, float f2, float f3, float f4)
	{
		head.rotateAngleY = f3 / 57.29578F;
		Chin.rotateAngleY = f3 / 57.29578F;
		righttooth.rotateAngleY = f3 / 57.29578F;
		lefttooth.rotateAngleY = f3 / 57.29578F;
		hair.rotateAngleY = f3 / 57.29578F;
		head.rotateAngleX = f4 / 57.29578F;
		Chin.rotateAngleX = f4 / 57.29578F;
		righttooth.rotateAngleX = f4 / 57.29578F;
		lefttooth.rotateAngleX = f4 / 57.29578F;
		hair.rotateAngleX = f4 / 57.29578F;
		rightarm.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * 2.0F * f1 * 0.5F;
		leftarm.rotateAngleX = MathHelper.cos(f * 0.6662F) * 2.0F * f1 * 0.5F;
		rightarm.rotateAngleZ = 0.0F;
		leftarm.rotateAngleZ = 0.0F;
		rightleg.rotateAngleX = MathHelper.cos(f * 0.6662F) * 1.4F * f1;
		rightfoot.rotateAngleX = MathHelper.cos(f * 0.6662F) * 1.4F * f1;
		leftleg.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * 1.4F * f1;
		leftfoot.rotateAngleX = MathHelper.cos(f * 0.6662F + 3.141593F) * 1.4F * f1;
		rightleg.rotateAngleY = 0.0F;
		rightfoot.rotateAngleY = 0.0F;
		leftleg.rotateAngleY = 0.0F;
		leftfoot.rotateAngleY = 0.0F;

		if (entity.getRidingEntity() != null)
		{
			rightarm.rotateAngleX += -0.6283185F;
			rightshoulder.rotateAngleX += -0.6283185F;
			leftarm.rotateAngleX += -0.6283185F;
			leftshoulder.rotateAngleX += -0.6283185F;
			rightleg.rotateAngleX = -1.256637F;
			rightfoot.rotateAngleX = -1.256637F;
			leftleg.rotateAngleX = -1.256637F;
			leftfoot.rotateAngleX = -1.256637F;
			rightleg.rotateAngleY = 0.3141593F;
			rightfoot.rotateAngleY = 0.3141593F;
			leftleg.rotateAngleY = -0.3141593F;
			leftfoot.rotateAngleY = -0.3141593F;
		}
		if (field_1279_h)
		{
			leftarm.rotateAngleX = leftarm.rotateAngleX * 0.5F - 0.3141593F;
		}
		if (field_1278_i)
		{
			rightarm.rotateAngleX = rightarm.rotateAngleX * 0.5F - 0.3141593F;
		}
		rightarm.rotateAngleY = 0.0F;
		rightshoulder.rotateAngleY = 0.0F;
		leftarm.rotateAngleY = 0.0F;
		leftshoulder.rotateAngleY = 0.0F;
		float onGround = 0;
		if (onGround > -9990F)
		{
			float f6 = onGround;
			body.rotateAngleY = MathHelper.sin(MathHelper.sqrt(f6) * 3.141593F * 2.0F) * 0.2F;
			belly.rotateAngleY = MathHelper.sin(MathHelper.sqrt(f6) * 3.141593F * 2.0F) * 0.2F;
			rightarm.rotationPointZ = MathHelper.sin(body.rotateAngleY) * 5F;
			rightshoulder.rotationPointZ = MathHelper.sin(body.rotateAngleY) * 5F;
			rightarm.rotationPointX = -MathHelper.cos(body.rotateAngleY) * 5F;
			rightshoulder.rotationPointX = -MathHelper.cos(body.rotateAngleY) * 5F;
			leftarm.rotationPointZ = -MathHelper.sin(body.rotateAngleY) * 5F;
			leftshoulder.rotationPointZ = -MathHelper.sin(body.rotateAngleY) * 5F;
			leftarm.rotationPointX = MathHelper.cos(body.rotateAngleY) * 5F;
			leftshoulder.rotationPointX = MathHelper.cos(body.rotateAngleY) * 5F;
			rightarm.rotateAngleY += body.rotateAngleY;
			rightshoulder.rotateAngleY += body.rotateAngleY;
			leftarm.rotateAngleY += body.rotateAngleY;
			leftshoulder.rotateAngleY += body.rotateAngleY;
			leftarm.rotateAngleX += body.rotateAngleY;
			leftshoulder.rotateAngleX += body.rotateAngleY;
			f6 = 1.0F - onGround;
			f6 *= f6;
			f6 *= f6;
			f6 = 1.0F - f6;
			float f7 = MathHelper.sin(f6 * 3.141593F);
			float f8 = MathHelper.sin(onGround * 3.141593F) * -(head.rotateAngleX - 0.7F) * 0.75F;
			rightarm.rotateAngleX = (float) ((double) rightarm.rotateAngleX - ((double) f7 * 1.2D + (double) f8));
			rightshoulder.rotateAngleX = (float) ((double) rightarm.rotateAngleX - ((double) f7 * 1.2D + (double) f8));
			leftshoulder.rotateAngleX = (float) ((double) leftarm.rotateAngleX - ((double) f7 * 1.2D + (double) f8));
			rightarm.rotateAngleY += body.rotateAngleY * 2.0F;
			rightshoulder.rotateAngleY += body.rotateAngleY * 2.0F;
			rightarm.rotateAngleZ = MathHelper.sin(onGround * 3.141593F) * -0.4F;
			rightshoulder.rotateAngleZ = MathHelper.sin(onGround * 3.141593F) * -0.4F;
		}
		if (isSneak)
		{
			body.rotateAngleX = 0.5F;
			belly.rotateAngleX = 0.5F;
			rightleg.rotateAngleX -= 0.0F;
			rightfoot.rotateAngleX -= 0.0F;
			leftleg.rotateAngleX -= 0.0F;
			leftfoot.rotateAngleX -= 0.0F;
			rightarm.rotateAngleX += 0.4F;
			rightshoulder.rotateAngleX += 0.4F;
			leftarm.rotateAngleX += 0.4F;
			leftshoulder.rotateAngleX += 0.4F;
			rightleg.rotationPointZ = 4F;
			rightfoot.rotationPointZ = 4F;
			leftleg.rotationPointZ = 4F;
			leftfoot.rotationPointZ = 4F;
			rightleg.rotationPointY = 9F;
			rightfoot.rotationPointY = 9F;
			leftleg.rotationPointY = 9F;
			leftfoot.rotationPointY = 9F;
			head.rotationPointY = 1.0F;
			Chin.rotationPointY = 1.0F;
			righttooth.rotationPointY = 1.0F;
			lefttooth.rotationPointY = 1.0F;
			hair.rotationPointY = 1.0F;
		}
		else
		{
			body.rotateAngleX = 0.0F;
			belly.rotateAngleX = 0.0F;
			rightleg.rotationPointZ = 0.0F;
			rightfoot.rotationPointZ = 0.0F;
			leftleg.rotationPointZ = 0.0F;
			leftfoot.rotationPointZ = 0.0F;
			rightleg.rotationPointY = 12F;
			rightfoot.rotationPointY = 12F;
			leftleg.rotationPointY = 12F;
			leftfoot.rotationPointY = 12F;
			head.rotationPointY = 0.0F;
			righttooth.rotationPointY = 0.0F;
			lefttooth.rotationPointY = 0.0F;
			hair.rotationPointY = 0.0F;
			Chin.rotationPointY = 0.0F;
		}
		rightarm.rotateAngleZ += MathHelper.cos(f2 * 0.09F) * 0.05F + 0.05F;
		leftarm.rotateAngleZ -= MathHelper.cos(f2 * 0.09F) * 0.05F + 0.05F;
		rightarm.rotateAngleX += MathHelper.sin(f2 * 0.067F) * 0.05F;
		leftarm.rotateAngleX -= MathHelper.sin(f2 * 0.067F) * 0.05F;
	}

	public boolean field_1279_h;
	public boolean field_1278_i;
	public boolean isSneak;
	public ModelRenderer body;
	public ModelRenderer belly;
	public ModelRenderer hair;
	public ModelRenderer head;
	public ModelRenderer rightarm;
	public ModelRenderer leftarm;
	public ModelRenderer leftfoot;
	public ModelRenderer leftshoulder;
	public ModelRenderer rightshoulder;
	public ModelRenderer Chin;
	public ModelRenderer righttooth;
	public ModelRenderer lefttooth;
	public ModelRenderer leftleg;
	public ModelRenderer rightleg;
	public ModelRenderer rightfoot;

}
