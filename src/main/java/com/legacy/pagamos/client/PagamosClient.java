package com.legacy.pagamos.client;

import com.legacy.pagamos.client.render.PagamosEntityRendering;
import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class PagamosClient
{

	public static void initialization(FMLClientSetupEvent event)
	{
		PagamosEntityRendering.init();

		renderTranslucent(PagamosBlocks.pagamos_portal);
		renderTranslucent(PagamosBlocks.ice_bricks);
		renderTranslucent(PagamosBlocks.crystal_ice);
		// renderCutout(PagamosBlocks.cicle);
		renderCutout(PagamosBlocks.blue_fire);

	}

	private static void renderCutout(Block block)
	{
		RenderTypeLookup.setRenderLayer(block, RenderType.getCutout());
	}

	private static void renderTranslucent(Block block)
	{
		RenderTypeLookup.setRenderLayer(block, RenderType.getTranslucent());
	}
}