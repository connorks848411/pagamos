package com.legacy.pagamos;

import com.legacy.pagamos.registry.PagamosDimensions;

import net.minecraftforge.event.world.RegisterDimensionsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class PagamosEvents
{
	@SubscribeEvent
	public void onRegisteredDimension(RegisterDimensionsEvent event)
	{
		PagamosDimensions.initDimensions();
	}
}
