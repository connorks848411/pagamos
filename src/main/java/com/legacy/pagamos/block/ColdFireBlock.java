package com.legacy.pagamos.block;

import java.util.Random;

import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.FireBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;

public class ColdFireBlock extends FireBlock
{
	public ColdFireBlock(Properties builder)
	{
		super(builder);
	}

	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
	{
		if (worldIn.getBlockState(pos.down()).getBlock() == PagamosBlocks.ice_cobblestone)
			return;

		super.tick(state, worldIn, pos, rand);
	}
}
