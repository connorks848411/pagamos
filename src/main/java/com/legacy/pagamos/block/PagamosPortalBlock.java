package com.legacy.pagamos.block;

import java.util.Random;

import javax.annotation.Nullable;

import com.google.common.cache.LoadingCache;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosDimensions;
import com.legacy.pagamos.registry.PagamosSounds;
import com.legacy.pagamos.util.ObfuscationShortcuts;
import com.legacy.pagamos.world.teleporter.PagamosTeleporter;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.NetherPortalBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.CachedBlockInfo;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class PagamosPortalBlock extends NetherPortalBlock
{
	private static final java.lang.reflect.Field defaultTeleporter = ObfuscationReflectionHelper.findField(ServerWorld.class, "field_85177_Q"); // worldTeleporter

	public PagamosPortalBlock()
	{
		super(Block.Properties.create(Material.PORTAL).doesNotBlockMovement().tickRandomly().hardnessAndResistance(-1.0F).sound(SoundType.GLASS).lightValue(11).noDrops());
	}

	@Override
	public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random)
	{
		super.tick(state, worldIn, pos, random);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		if (rand.nextInt(100) == 0)
		{
			worldIn.playSound((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, PagamosSounds.pagamos_portal, SoundCategory.BLOCKS, 0.5F, rand.nextFloat() * 0.4F + 0.8F, false);
		}

		for (int i = 0; i < 4; ++i)
		{
			double d0 = (double) ((float) pos.getX() + rand.nextFloat());
			double d1 = (double) ((float) pos.getY() + rand.nextFloat());
			double d2 = (double) ((float) pos.getZ() + rand.nextFloat());
			double d3 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			double d4 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			double d5 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			int j = rand.nextInt(2) * 2 - 1;

			if (worldIn.getBlockState(pos.west()).getBlock() != this && worldIn.getBlockState(pos.east()).getBlock() != this)
			{
				d0 = (double) pos.getX() + 0.5D + 0.25D * (double) j;
				d3 = (double) (rand.nextFloat() * 2.0F * (float) j);
			}
			else
			{
				d2 = (double) pos.getZ() + 0.5D + 0.25D * (double) j;
				d5 = (double) (rand.nextFloat() * 2.0F * (float) j);
			}

			worldIn.addParticle(ParticleTypes.POOF, d0, d1, d2, d3 / 4D, d4 / 4D, d5 / 4D);
		}
	}

	@Override
	public void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entity)
	{
		if (!world.isRemote() && canEntityTravel(entity))
		{
			if (entity.timeUntilPortal > 0)
				entity.timeUntilPortal = entity.getPortalCooldown();
			else
			{
				DimensionType destDim;
				if (world.getDimension().getType() == DimensionType.OVERWORLD)
					destDim = PagamosDimensions.pagamosType();
				else
					destDim = DimensionType.OVERWORLD;

				ServerWorld destWorld = world.getServer().getWorld(destDim);
				Teleporter oldDefaultTp = destWorld.getDefaultTeleporter();

				ObfuscationShortcuts.setObfuscatedValue(defaultTeleporter, destWorld, new PagamosTeleporter(world.getServer().getWorld(destDim), this));
				PagamosTeleporter.setLastPortalVec(pos, entity, this);
				entity.timeUntilPortal = 500;
				entity.changeDimension(destDim);
				ObfuscationShortcuts.setObfuscatedValue(defaultTeleporter, destWorld, oldDefaultTp);
			}
		}
	}

	private boolean canEntityTravel(Entity entity)
	{
		return !entity.isBeingRidden() && entity.getRidingEntity() == null && entity.isNonBoss();
	}

	@Override
	public boolean trySpawnPortal(IWorld worldIn, BlockPos pos)
	{
		PagamosPortalBlock.Size blockportal$size = new PagamosPortalBlock.Size(worldIn, pos, Direction.Axis.X);

		if (blockportal$size.isValid() && blockportal$size.portalBlockCount == 0)
		{
			blockportal$size.placePortalBlocks();
			return true;
		}
		else
		{
			PagamosPortalBlock.Size blockportal$size1 = new PagamosPortalBlock.Size(worldIn, pos, Direction.Axis.Z);

			if (blockportal$size1.isValid() && blockportal$size1.portalBlockCount == 0)
			{
				blockportal$size1.placePortalBlocks();
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	@Override
	public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos)
	{
		Direction.Axis direction$axis = facing.getAxis();
		Direction.Axis direction$axis1 = stateIn.get(AXIS);
		boolean flag = direction$axis1 != direction$axis && direction$axis.isHorizontal();
		return !flag && facingState.getBlock() != PagamosBlocks.pagamos_portal && !(new PagamosPortalBlock.Size(worldIn, currentPos, direction$axis1)).func_208508_f() ? Blocks.AIR.getDefaultState() : super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
	}

	@Override
	public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		Direction.Axis enumfacing$axis = (Direction.Axis) state.get(AXIS);

		if (enumfacing$axis == Direction.Axis.X)
		{
			PagamosPortalBlock.Size blockportal$size = new PagamosPortalBlock.Size(worldIn, pos, Direction.Axis.X);

			if (!blockportal$size.isValid() || blockportal$size.portalBlockCount < blockportal$size.width * blockportal$size.height)
			{
				worldIn.destroyBlock(pos, false);
			}
		}
		else if (enumfacing$axis == Direction.Axis.Z)
		{
			PagamosPortalBlock.Size blockportal$size1 = new PagamosPortalBlock.Size(worldIn, pos, Direction.Axis.Z);

			if (!blockportal$size1.isValid() || blockportal$size1.portalBlockCount < blockportal$size1.width * blockportal$size1.height)
			{
				worldIn.destroyBlock(pos, false);
			}
		}
	}

	@SuppressWarnings("deprecation")
	// =@Override
	public static BlockPattern.PatternHelper createPatternHelper(IWorld worldIn, BlockPos p_181089_2_)
	{
		Direction.Axis direction$axis = Direction.Axis.Z;
		PagamosPortalBlock.Size netherportalblock$size = new PagamosPortalBlock.Size(worldIn, p_181089_2_, Direction.Axis.X);
		LoadingCache<BlockPos, CachedBlockInfo> loadingcache = BlockPattern.createLoadingCache(worldIn, true);
		if (!netherportalblock$size.isValid())
		{
			direction$axis = Direction.Axis.X;
			netherportalblock$size = new PagamosPortalBlock.Size(worldIn, p_181089_2_, Direction.Axis.Z);
		}
		if (!netherportalblock$size.isValid())
		{
			return new BlockPattern.PatternHelper(p_181089_2_, Direction.NORTH, Direction.UP, loadingcache, 1, 1, 1);
		}
		else
		{
			int[] aint = new int[Direction.AxisDirection.values().length];
			Direction direction = netherportalblock$size.rightDir.rotateYCCW();
			BlockPos blockpos = netherportalblock$size.bottomLeft.up(netherportalblock$size.getHeight() - 1);
			for (Direction.AxisDirection direction$axisdirection : Direction.AxisDirection.values())
			{
				BlockPattern.PatternHelper blockpattern$patternhelper = new BlockPattern.PatternHelper(direction.getAxisDirection() == direction$axisdirection ? blockpos : blockpos.offset(netherportalblock$size.rightDir, netherportalblock$size.getWidth() - 1), Direction.getFacingFromAxis(direction$axisdirection, direction$axis), Direction.UP, loadingcache, netherportalblock$size.getWidth(), netherportalblock$size.getHeight(), 1);
				for (int i = 0; i < netherportalblock$size.getWidth(); ++i)
				{
					for (int j = 0; j < netherportalblock$size.getHeight(); ++j)
					{
						CachedBlockInfo cachedblockinfo = blockpattern$patternhelper.translateOffset(i, j, 1);
						if (!cachedblockinfo.getBlockState().isAir())
						{
							++aint[direction$axisdirection.ordinal()];
						}
					}
				}
			}
			Direction.AxisDirection direction$axisdirection1 = Direction.AxisDirection.POSITIVE;
			for (Direction.AxisDirection direction$axisdirection2 : Direction.AxisDirection.values())
			{
				if (aint[direction$axisdirection2.ordinal()] < aint[direction$axisdirection1.ordinal()])
				{
					direction$axisdirection1 = direction$axisdirection2;
				}
			}
			return new BlockPattern.PatternHelper(direction.getAxisDirection() == direction$axisdirection1 ? blockpos : blockpos.offset(netherportalblock$size.rightDir, netherportalblock$size.getWidth() - 1), Direction.getFacingFromAxis(direction$axisdirection1, direction$axis), Direction.UP, loadingcache, netherportalblock$size.getWidth(), netherportalblock$size.getHeight(), 1);
		}
	}

	public static class Size
	{

		private final IWorld world;

		private final Direction.Axis axis;

		private final Direction rightDir;

		private final Direction leftDir;

		private int portalBlockCount;

		@Nullable
		private BlockPos bottomLeft;

		private int height;

		private int width;

		public Size(IWorld p_i48740_1_, BlockPos p_i48740_2_, Direction.Axis p_i48740_3_)
		{
			this.world = p_i48740_1_;
			this.axis = p_i48740_3_;

			if (p_i48740_3_ == Direction.Axis.X)
			{
				this.leftDir = Direction.EAST;
				this.rightDir = Direction.WEST;
			}
			else
			{
				this.leftDir = Direction.NORTH;
				this.rightDir = Direction.SOUTH;
			}
			for (BlockPos blockpos = p_i48740_2_; p_i48740_2_.getY() > blockpos.getY() - 21 && p_i48740_2_.getY() > 0 && this.func_196900_a(p_i48740_1_.getBlockState(p_i48740_2_.down())); p_i48740_2_ = p_i48740_2_.down())
			{
				;
			}
			int i = this.getDistanceUntilEdge(p_i48740_2_, this.leftDir) - 1;
			if (i >= 0)
			{
				this.bottomLeft = p_i48740_2_.offset(this.leftDir, i);
				this.width = this.getDistanceUntilEdge(this.bottomLeft, this.rightDir);
				if (this.width < 2 || this.width > 21)
				{
					this.bottomLeft = null;
					this.width = 0;
				}
			}
			if (this.bottomLeft != null)
			{
				this.height = this.calculatePortalHeight();
			}
		}

		protected int getDistanceUntilEdge(BlockPos p_180120_1_, Direction p_180120_2_)
		{
			int i;
			for (i = 0; i < 22; ++i)
			{
				BlockPos blockpos = p_180120_1_.offset(p_180120_2_, i);
				if (!this.func_196900_a(this.world.getBlockState(blockpos)) || this.world.getBlockState(blockpos.down()).getBlock() != Blocks.SNOW_BLOCK)
				{
					break;
				}
			}
			Block block = this.world.getBlockState(p_180120_1_.offset(p_180120_2_, i)).getBlock();
			return block == Blocks.SNOW_BLOCK ? i : 0;
		}

		public int getHeight()
		{
			return this.height;
		}

		public int getWidth()
		{
			return this.width;
		}

		protected int calculatePortalHeight()
		{
			label56: for (this.height = 0; this.height < 21; ++this.height)
			{
				for (int i = 0; i < this.width; ++i)
				{
					BlockPos blockpos = this.bottomLeft.offset(this.rightDir, i).up(this.height);
					BlockState blockstate = this.world.getBlockState(blockpos);
					if (!this.func_196900_a(blockstate))
					{
						break label56;
					}
					Block block = blockstate.getBlock();
					if (block == PagamosBlocks.pagamos_portal)
					{
						++this.portalBlockCount;
					}
					if (i == 0)
					{
						block = this.world.getBlockState(blockpos.offset(this.leftDir)).getBlock();
						if (block != Blocks.SNOW_BLOCK)
						{
							break label56;
						}
					}
					else if (i == this.width - 1)
					{
						block = this.world.getBlockState(blockpos.offset(this.rightDir)).getBlock();
						if (block != Blocks.SNOW_BLOCK)
						{
							break label56;
						}
					}
				}
			}
			for (int j = 0; j < this.width; ++j)
			{
				if (this.world.getBlockState(this.bottomLeft.offset(this.rightDir, j).up(this.height)).getBlock() != Blocks.SNOW_BLOCK)
				{
					this.height = 0;
					break;
				}
			}
			if (this.height <= 21 && this.height >= 3)
			{
				return this.height;
			}
			else
			{
				this.bottomLeft = null;
				this.width = 0;
				this.height = 0;
				return 0;
			}
		}

		@SuppressWarnings("deprecation")
		protected boolean func_196900_a(BlockState p_196900_1_)
		{
			Block block = p_196900_1_.getBlock();
			return p_196900_1_.isAir() || block == Blocks.FIRE || block == PagamosBlocks.pagamos_portal || block == Blocks.AIR || block == PagamosBlocks.blue_fire;
		}

		public boolean isValid()
		{
			return this.bottomLeft != null && this.width >= 2 && this.width <= 21 && this.height >= 3 && this.height <= 21;
		}

		public void placePortalBlocks()
		{
			for (int i = 0; i < this.width; ++i)
			{
				BlockPos blockpos = this.bottomLeft.offset(this.rightDir, i);
				for (int j = 0; j < this.height; ++j)
				{
					this.world.setBlockState(blockpos.up(j), PagamosBlocks.pagamos_portal.getDefaultState().with(PagamosPortalBlock.AXIS, this.axis), 18);
				}
			}
		}

		private boolean func_196899_f()
		{
			return this.portalBlockCount >= this.width * this.height;
		}

		public boolean func_208508_f()
		{
			return this.isValid() && this.func_196899_f();
		}
	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack)
	{
		BlockState frame_block = Blocks.SNOW_BLOCK.getDefaultState();

		for (int y = -1; y <= 3; ++y)
		{
			if (placer.getHorizontalFacing() == Direction.NORTH || placer.getHorizontalFacing() == Direction.SOUTH)
			{
				for (int x = -1; x <= 2; ++x)
				{
					worldIn.setBlockState(pos.add(x, y, 0), frame_block, 18);
				}
			}
			else
			{
				for (int z = -1; z <= 2; ++z)
				{
					worldIn.setBlockState(pos.add(0, y, z), frame_block, 18);
				}
			}
		}

		for (int y1 = 0; y1 <= 2; ++y1)
		{
			if (placer.getHorizontalFacing() == Direction.NORTH || placer.getHorizontalFacing() == Direction.SOUTH)
			{
				for (int x1 = 0; x1 <= 1; ++x1)
				{
					worldIn.setBlockState(pos.add(x1, y1, 0), PagamosBlocks.pagamos_portal.getDefaultState().with(AXIS, Direction.Axis.X), 18);
				}
			}
			else
			{
				for (int z1 = 0; z1 <= 1; ++z1)
				{
					worldIn.setBlockState(pos.add(0, y1, z1), PagamosBlocks.pagamos_portal.getDefaultState().with(AXIS, Direction.Axis.Z), 18);
				}
			}
		}

	}

	public PagamosPortalBlock.Size isSkyPortal(IWorld p_201816_1_, BlockPos p_201816_2_)
	{
		PagamosPortalBlock.Size netherportalblock$size = new PagamosPortalBlock.Size(p_201816_1_, p_201816_2_, Direction.Axis.X);
		if (netherportalblock$size.isValid() && netherportalblock$size.portalBlockCount == 0)
		{
			return netherportalblock$size;
		}
		else
		{
			PagamosPortalBlock.Size netherportalblock$size1 = new PagamosPortalBlock.Size(p_201816_1_, p_201816_2_, Direction.Axis.Z);
			return netherportalblock$size1.isValid() && netherportalblock$size1.portalBlockCount == 0 ? netherportalblock$size1 : null;
		}
	}
}