package com.legacy.pagamos.registry;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.PagamosRegistry;
import com.legacy.pagamos.world.biome.features.CrystalBlobFeature;

import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(PagamosMod.MODID)
public class PagamosFeatures
{
	public static final Feature<NoFeatureConfig> CRYSTAL_BLOB = new CrystalBlobFeature(NoFeatureConfig::deserialize);

	public static void init(Register<Feature<?>> event)
	{
		PagamosRegistry.register(event.getRegistry(), "crystal_blob", CRYSTAL_BLOB);
	}
}
