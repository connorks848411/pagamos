package com.legacy.pagamos.registry;

import com.legacy.pagamos.PagamosMod;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class PagamosSounds
{
	// Blocks
	public static SoundEvent pagamos_portal, portal_travel, portal_create, portal_break, portal_trigger;

	public static SoundEvent yeti_idle, yeti_curious, yeti_hurt, yeti_death;

	public static SoundEvent phoenix_idle, phoenix_attack, phoenix_death;

	public static SoundEvent ice_crack;

	public static IForgeRegistry<SoundEvent> soundRegistry;

	public static void init()
	{
		pagamos_portal = register("block.pagamos.portal");
		portal_travel = register("block.pagamos.portal.travel");
		portal_create = register("block.pagamos.portal.create");
		portal_break = register("block.pagamos.portal.break");
		portal_trigger = register("block.pagamos.portal.trigger");

		// yeti_idle = register("entity.yeti.idle");
		// yeti_curious = register("entity.yeti.curious");
		// yeti_hurt = register("entity.yeti.hurt");
		// yeti_death = register("entity.yeti.death");

		phoenix_idle = register("entity.phoenix.idle");
		phoenix_attack = register("entity.phoenix.attack");
		phoenix_death = register("entity.phoenix.death");

		ice_crack = register("random.ice.crack");
	}

	private static SoundEvent register(String name)
	{
		ResourceLocation location = PagamosMod.locate(name);

		SoundEvent sound = new SoundEvent(location);

		sound.setRegistryName(location);

		if (soundRegistry != null)
			soundRegistry.register(sound);

		return sound;
	}

}