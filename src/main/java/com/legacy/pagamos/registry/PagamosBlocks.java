package com.legacy.pagamos.registry;

import java.util.LinkedHashMap;
import java.util.Map;

import com.legacy.pagamos.PagamosRegistry;
import com.legacy.pagamos.block.ColdFireBlock;
import com.legacy.pagamos.block.PagamosBreakableBlock;
import com.legacy.pagamos.block.PagamosPortalBlock;
import com.legacy.pagamos.item.PagamosItemGroup;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.FurnaceBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;

public class PagamosBlocks
{

	public static Block crystal_ice, crystal_stone;

	public static Block marble, ice_bricks, packed_snow;

	public static Block freezer, freezer_on, pagamos_portal, ice_cobblestone;

	public static Block cicle, creeper_ice;

	public static Block crystal_water, crystal_water_flowing;

	public static Block frozen_amethyst_ore, frozen_sapphire_ore, amethyst_ore, sapphire_ore, amethyst_block, sapphire_block;

	public static Block blue_fire;

	public static Map<Block, ItemGroup> blockItemMap = new LinkedHashMap<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	private static IForgeRegistry<Block> iBlockRegistry;

	public static void init(Register<Block> event)
	{
		PagamosBlocks.iBlockRegistry = event.getRegistry();

		pagamos_portal = register("pagamos_portal", new PagamosPortalBlock());

		packed_snow = register("packed_snow", new Block(Block.Properties.create(Material.ROCK).hardnessAndResistance(2.0F, 0.2F).sound(SoundType.SNOW).harvestTool(ToolType.PICKAXE)));
		crystal_ice = register("crystal_ice", new PagamosBreakableBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(0.5F, 0.4F).sound(SoundType.GLASS).lightValue(9).slipperiness(1.0F).notSolid()));
		ice_cobblestone = register("ice_cobblestone", new Block(Block.Properties.from(Blocks.COBBLESTONE)));
		crystal_stone = register("crystal_stone", new Block(Block.Properties.from(Blocks.GLOWSTONE)));
		marble = register("marble", new Block(Block.Properties.from(Blocks.STONE)));

		ice_bricks = register("ice_bricks", new PagamosBreakableBlock(Block.Properties.from(Blocks.PACKED_ICE).notSolid()));

		blue_fire = register("blue_fire", new ColdFireBlock(Block.Properties.from(Blocks.FIRE)), null);

		amethyst_block = register("amethyst_block", new Block(Block.Properties.from(Blocks.IRON_BLOCK)));
		sapphire_block = register("sapphire_block", new Block(Block.Properties.from(Blocks.IRON_BLOCK)));

		frozen_amethyst_ore = register("frozen_amethyst_ore", new Block(Block.Properties.from(Blocks.IRON_ORE).sound(SoundType.SNOW)));
		frozen_sapphire_ore = register("frozen_sapphire_ore", new Block(Block.Properties.from(Blocks.IRON_ORE).sound(SoundType.SNOW)));
		amethyst_ore = register("amethyst_ore", new Block(Block.Properties.from(Blocks.IRON_ORE)));
		sapphire_ore = register("sapphire_ore", new Block(Block.Properties.from(Blocks.IRON_ORE)));

		/*creeper_ice = register("creeper_ice", new CreeperIceBlock(Block.Properties.from(Blocks.ICE).hardnessAndResistance(0.2F, 0.0F)));*/

		freezer = register("freezer", new FurnaceBlock(Block.Properties.from(Blocks.FURNACE)) {});

		/*crystal_water = register("crystal_water", new BlockCrystalWater()).setCreativeTab(null);
		crystal_water_flowing = register("crystal_water_flowing", new BlockCrystalWaterFlowing()).setCreativeTab(null);*/
	}

	public static void setBlockRegistry(IForgeRegistry<Block> iBlockRegistry)
	{
		PagamosBlocks.iBlockRegistry = iBlockRegistry;
	}

	public static Block register(String name, Block block)
	{
		register(name, block, PagamosItemGroup.PAGAMOS);
		return block;
	}

	public static <T extends ItemGroup> Block register(String key, Block block, T itemGroup)
	{
		blockItemMap.put(block, itemGroup);
		return registerBlock(key, block);
	}

	public static Block registerBlock(String name, Block block)
	{
		if (iBlockRegistry != null)
		{
			PagamosRegistry.register(iBlockRegistry, name, block);
		}

		return block;
	}
}
