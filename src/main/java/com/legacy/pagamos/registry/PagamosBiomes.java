package com.legacy.pagamos.registry;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.PagamosRegistry;
import com.legacy.pagamos.world.biome.FrozenHellBiome;

import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(PagamosMod.MODID)
public class PagamosBiomes
{
	public static final Biome FROZEN_HELL = new FrozenHellBiome();

	public static void init(Register<Biome> event)
	{
		register(event.getRegistry(), "frozen_hell", FROZEN_HELL);

		BiomeDictionary.addTypes(FROZEN_HELL, BiomeDictionary.Type.COLD);
	}

	public static void register(IForgeRegistry<Biome> registry, String key, Biome biome)
	{
		PagamosRegistry.register(registry, key, biome);
	}
}
