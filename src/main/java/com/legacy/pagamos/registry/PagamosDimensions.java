package com.legacy.pagamos.registry;

import java.util.function.BiFunction;

import com.legacy.pagamos.PagamosMod;
import com.legacy.pagamos.PagamosRegistry;
import com.legacy.pagamos.world.PagamosDimension;

import io.netty.buffer.Unpooled;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.registries.IForgeRegistry;

public class PagamosDimensions
{
	private static final ModDimension PAGAMOS_DIMENSION = new ModDimension()
	{

		@Override
		public BiFunction<World, DimensionType, ? extends Dimension> getFactory()
		{
			return PagamosDimension::new;
		}
	};

	public static void initModDimensions(IForgeRegistry<ModDimension> registry)
	{
		PagamosRegistry.register(registry, "pagamos", PAGAMOS_DIMENSION);
		DimensionManager.registerDimension(PagamosMod.locate("pagamos"), PAGAMOS_DIMENSION, new PacketBuffer(Unpooled.buffer()), true);
	}

	public static void initDimensions()
	{
		if (PagamosDimensions.pagamosType() == null)
		{
			DimensionManager.registerDimension(PagamosMod.locate("good_dream"), PAGAMOS_DIMENSION, new PacketBuffer(Unpooled.buffer()), true);
		}
	}

	public static DimensionType pagamosType()
	{
		return DimensionType.byName(new ResourceLocation(PagamosMod.MODID, "pagamos"));
	}
}
