package com.legacy.pagamos;

import java.util.Map.Entry;

import com.legacy.pagamos.entity.PagamosEntityTypes;
import com.legacy.pagamos.item.PagamosItems;
import com.legacy.pagamos.registry.PagamosBiomes;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosDimensions;
import com.legacy.pagamos.registry.PagamosFeatures;
import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.Feature;
import net.minecraftforge.common.ModDimension;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = PagamosMod.MODID, bus = Bus.MOD)
public class PagamosRegistry
{

	@SubscribeEvent
	public static void onRegisterSounds(RegistryEvent.Register<SoundEvent> event)
	{
		PagamosSounds.soundRegistry = event.getRegistry();
		PagamosSounds.init();
	}

	@SubscribeEvent
	public static void onRegisterBlocks(RegistryEvent.Register<Block> event)
	{
		PagamosBlocks.init(event);
	}

	@SubscribeEvent
	public static void onRegisterItems(RegistryEvent.Register<Item> event)
	{
		PagamosItems.init(event);

		for (Entry<Block, ItemGroup> entry : PagamosBlocks.blockItemMap.entrySet())
		{
			PagamosRegistry.register(event.getRegistry(), entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), new Item.Properties().group(entry.getValue())));
		}
		PagamosBlocks.blockItemMap.clear();

		for (Entry<Block, Item.Properties> entry : PagamosBlocks.blockItemPropertiesMap.entrySet())
		{
			PagamosRegistry.register(event.getRegistry(), entry.getKey().getRegistryName().getPath(), new BlockItem(entry.getKey(), entry.getValue()));
		}
		PagamosBlocks.blockItemPropertiesMap.clear();
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		PagamosEntityTypes.init(event);
	}

	/*@SubscribeEvent
	public static void registerTileEntityTypes(Register<TileEntityType<?>> event)
	{
		PagamosTileEntityTypes.init(event);
	}*/

	@SubscribeEvent
	public static void onRegisterBiomes(Register<Biome> event)
	{
		PagamosBiomes.init(event);
	}

	@SubscribeEvent
	public static void registerFeatures(Register<Feature<?>> event)
	{
		PagamosFeatures.init(event);
	}

	@SubscribeEvent
	public static void onRegisterModDimensions(RegistryEvent.Register<ModDimension> event)
	{
		PagamosDimensions.initModDimensions(event.getRegistry());
	}

	public static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(PagamosMod.locate(name));
		registry.register(object);
	}
}