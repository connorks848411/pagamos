package com.legacy.pagamos.util;

public class ObfuscationShortcuts
{
	public static void setObfuscatedValue(java.lang.reflect.Field field, Object clazz, Object changeTo)
	{
		try
		{
			field.set(clazz, changeTo);
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
}
