package com.legacy.pagamos;

import com.legacy.pagamos.client.PagamosClient;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(PagamosMod.MODID)
public class PagamosMod
{
	public static final String NAME = "Pagamos";
	public static final String MODID = "pagamos";

	public PagamosMod()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initialization);

		DistExecutor.runWhenOn(Dist.CLIENT, () -> () ->
		{
			FMLJavaModLoadingContext.get().getModEventBus().addListener(PagamosClient::initialization);
			MinecraftForge.EVENT_BUS.register(new PagamosClient());
		});
	}

	private void initialization(final FMLCommonSetupEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new PagamosEvents());
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}
}
