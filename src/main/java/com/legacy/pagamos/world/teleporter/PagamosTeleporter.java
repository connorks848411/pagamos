package com.legacy.pagamos.world.teleporter;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;
import com.legacy.pagamos.block.PagamosPortalBlock;
import com.legacy.pagamos.util.ObfuscationShortcuts;

import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.pattern.BlockPattern;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.Teleporter;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class PagamosTeleporter extends Teleporter
{
	protected final Map<BlockPos, PagamosTeleporter.PortalPosition> destinationCoordinateCache = Maps.newHashMapWithExpectedSize(4096);
	private final Object2LongMap<BlockPos> columnPosMap = new Object2LongOpenHashMap<>();
	private final PagamosPortalBlock portalBlock;

	public PagamosTeleporter(ServerWorld worldIn, PagamosPortalBlock portalBlock)
	{
		super(worldIn);
		this.portalBlock = portalBlock;
	}

	@Override
	public boolean placeInPortal(Entity entity, float yaw)
	{
		BlockPattern.PatternHelper patternHelper = PagamosPortalBlock.createPatternHelper(this.world, entity.getPosition());
		double d0 = patternHelper.getForwards().getAxis() == Direction.Axis.X ? (double) patternHelper.getFrontTopLeft().getZ() : (double) patternHelper.getFrontTopLeft().getX();
		double d1 = Math.abs(MathHelper.pct((patternHelper.getForwards().getAxis() == Direction.Axis.X ? entity.getPosX() : entity.getPosX()) - (double) (patternHelper.getForwards().rotateY().getAxisDirection() == Direction.AxisDirection.NEGATIVE ? 1 : 0), d0, d0 - (double) patternHelper.getWidth()));
		double d2 = MathHelper.pct(entity.getPosY() - 1.0D, (double) patternHelper.getFrontTopLeft().getY(), (double) (patternHelper.getFrontTopLeft().getY() - patternHelper.getHeight()));
		Vec3d posVec = new Vec3d(d1, d2, 0.0D);
		Direction direction = patternHelper.getForwards();

		BlockPattern.PortalInfo portalInfo = this.placeInExistingPortal(new BlockPos(entity), entity.getMotion(), direction, posVec.x, posVec.y, entity instanceof PlayerEntity);
		if (portalInfo == null)
		{
			return false;
		}
		else
		{
			Vec3d portalPos = portalInfo.pos;
			Vec3d portalMotion = portalInfo.motion;
			entity.setMotion(portalMotion);
			entity.rotationYaw = yaw + (float) portalInfo.rotation;

			if (entity instanceof ServerPlayerEntity)
			{
				((ServerPlayerEntity) entity).connection.setPlayerLocation(portalPos.x, portalPos.y, portalPos.z, entity.rotationYaw, entity.rotationPitch);
				((ServerPlayerEntity) entity).connection.captureCurrentPosition();
			}
			else
			{
				entity.setLocationAndAngles(portalPos.x, portalPos.y, portalPos.z, entity.rotationYaw, entity.rotationPitch);
			}
			if (entity instanceof LivingEntity)
			{
				entity.hurtResistantTime = 20;
			}

			return true;
		}
	}

	public static java.lang.reflect.Field lastPortalVec = ObfuscationReflectionHelper.findField(Entity.class, "field_181017_ao"); // lastPortalVec

	public static void setLastPortalVec(BlockPos pos, Entity entity, PagamosPortalBlock portal)
	{
		BlockPattern.PatternHelper patternHelper = PagamosPortalBlock.createPatternHelper(entity.getEntityWorld(), pos);
		double d0 = patternHelper.getForwards().getAxis() == Direction.Axis.X ? (double) patternHelper.getFrontTopLeft().getZ() : (double) patternHelper.getFrontTopLeft().getX();
		double d1 = Math.abs(MathHelper.pct((patternHelper.getForwards().getAxis() == Direction.Axis.X ? entity.getPosZ() : entity.getPosX()) - (double) (patternHelper.getForwards().rotateY().getAxisDirection() == Direction.AxisDirection.NEGATIVE ? 1 : 0), d0, d0 - (double) patternHelper.getWidth()));
		double d2 = MathHelper.pct(entity.getPosY() - 1.0D, (double) patternHelper.getFrontTopLeft().getY(), (double) (patternHelper.getFrontTopLeft().getY() - patternHelper.getHeight()));
		ObfuscationShortcuts.setObfuscatedValue(lastPortalVec, entity, new Vec3d(d1, d2, 0.0D));
	}

	@Override
	@Nullable
	public BlockPattern.PortalInfo placeInExistingPortal(BlockPos pos, Vec3d motionVec, Direction directionIn, double xIn, double yIn, boolean isPlayer)
	{
		boolean flag = true;
		BlockPos blockPos = null;
		BlockPos columnpos = new BlockPos(pos);
		if (!isPlayer && this.columnPosMap.containsKey(columnpos))
		{
			return null;
		}
		else
		{
			PagamosTeleporter.PortalPosition portalPos = this.destinationCoordinateCache.get(columnpos);
			if (portalPos != null)
			{
				blockPos = portalPos.pos;
				portalPos.lastUpdateTime = this.world.getGameTime();
				flag = false;
			}
			else
			{
				int range = 32;
				BlockPos prevPos = null;
				for (int x = -range; x <= range; ++x)
					for (int z = -range; z <= range; ++z)
						for (int y = 0; y < 255; y++)
						{
							BlockPos currentPos = new BlockPos(pos.getX() + x, y, pos.getZ() + z);
							if (world.getBlockState(currentPos).getBlock() == portalBlock && world.getBlockState(currentPos.down()).getBlock() != portalBlock)
							{
								if (prevPos == null)
									prevPos = currentPos;
								else if (getDistance(prevPos, pos) > getDistance(currentPos, pos))
									prevPos = currentPos;
							}
						}
				blockPos = prevPos;
			}

			if (blockPos == null)
			{
				long l = this.world.getGameTime() + 300L;
				this.columnPosMap.put(columnpos, l);
				return null;
			}
			else
			{
				if (flag)
				{
					this.destinationCoordinateCache.put(columnpos, new PagamosTeleporter.PortalPosition(blockPos, this.world.getGameTime()));
					this.world.getChunkProvider().registerTicket(TicketType.PORTAL, new ChunkPos(blockPos), 3, columnpos);
				}

				BlockPattern.PatternHelper patternHelper = PagamosPortalBlock.createPatternHelper(this.world, blockPos);
				return patternHelper.getPortalInfo(directionIn, blockPos, yIn, motionVec, xIn);
			}
		}
	}

	@Override
	public boolean makePortal(Entity entityIn)
	{
		BlockPos pos = entityIn.getPosition();
		double d0 = -1.0D;
		int x = MathHelper.floor(pos.getX());
		int y = MathHelper.floor(pos.getY());
		int z = MathHelper.floor(pos.getZ());
		int i1 = x;
		int j1 = y;
		int k1 = z;
		int l1 = 0;
		int i2 = world.rand.nextInt(4);
		BlockPos.Mutable blockpos$mutableblockpos = new BlockPos.Mutable();

		for (int j2 = x - 16; j2 <= x + 16; ++j2)
		{
			double d1 = (double) j2 + 0.5D - pos.getX();

			for (int l2 = z - 16; l2 <= z + 16; ++l2)
			{
				double d2 = (double) l2 + 0.5D - pos.getZ();

				label276: for (int j3 = world.getActualHeight() - 1; j3 >= 0; --j3)
				{
					if (world.isAirBlock(blockpos$mutableblockpos.setPos(j2, j3, l2)))
					{
						while (j3 > 0 && world.isAirBlock(blockpos$mutableblockpos.setPos(j2, j3 - 1, l2)))
						{
							--j3;
						}

						for (int k3 = i2; k3 < i2 + 4; ++k3)
						{
							int l3 = k3 % 2;
							int i4 = 1 - l3;
							if (k3 % 4 >= 2)
							{
								l3 = -l3;
								i4 = -i4;
							}

							for (int j4 = 0; j4 < 3; ++j4)
							{
								for (int k4 = 0; k4 < 4; ++k4)
								{
									for (int l4 = -1; l4 < 4; ++l4)
									{
										int i5 = j2 + (k4 - 1) * l3 + j4 * i4;
										int j5 = j3 + l4;
										int k5 = l2 + (k4 - 1) * i4 - j4 * l3;
										blockpos$mutableblockpos.setPos(i5, j5, k5);
										if (l4 < 0 && !world.getBlockState(blockpos$mutableblockpos).getMaterial().isSolid() || l4 >= 0 && !world.isAirBlock(blockpos$mutableblockpos))
										{
											continue label276;
										}
									}
								}
							}

							double d5 = (double) j3 + 0.5D - pos.getY();
							double d7 = d1 * d1 + d5 * d5 + d2 * d2;
							if (d0 < 0.0D || d7 < d0)
							{
								d0 = d7;
								i1 = j2;
								j1 = j3;
								k1 = l2;
								l1 = k3 % 4;
							}
						}
					}
				}
			}
		}

		if (d0 < 0.0D)
		{
			for (int l5 = x - 16; l5 <= x + 16; ++l5)
			{
				double d3 = (double) l5 + 0.5D - pos.getX();

				for (int j6 = z - 16; j6 <= z + 16; ++j6)
				{
					double d4 = (double) j6 + 0.5D - pos.getY();

					label214: for (int i7 = world.getActualHeight() - 1; i7 >= 0; --i7)
					{
						if (world.isAirBlock(blockpos$mutableblockpos.setPos(l5, i7, j6)))
						{
							while (i7 > 0 && world.isAirBlock(blockpos$mutableblockpos.setPos(l5, i7 - 1, j6)))
							{
								--i7;
							}

							for (int l7 = i2; l7 < i2 + 2; ++l7)
							{
								int l8 = l7 % 2;
								int k9 = 1 - l8;

								for (int i10 = 0; i10 < 4; ++i10)
								{
									for (int k10 = -1; k10 < 4; ++k10)
									{
										int i11 = l5 + (i10 - 1) * l8;
										int j11 = i7 + k10;
										int k11 = j6 + (i10 - 1) * k9;
										blockpos$mutableblockpos.setPos(i11, j11, k11);
										if (k10 < 0 && !world.getBlockState(blockpos$mutableblockpos).getMaterial().isSolid() || k10 >= 0 && !world.isAirBlock(blockpos$mutableblockpos))
										{
											continue label214;
										}
									}
								}

								double d6 = (double) i7 + 0.5D - pos.getY();
								double d8 = d3 * d3 + d6 * d6 + d4 * d4;
								if (d0 < 0.0D || d8 < d0)
								{
									d0 = d8;
									i1 = l5;
									j1 = i7;
									k1 = j6;
									l1 = l7 % 2;
								}
							}
						}
					}
				}
			}
		}

		int i6 = i1;
		int k2 = j1;
		int k6 = k1;
		int l6 = l1 % 2;
		int i3 = 1 - l6;
		if (l1 % 4 >= 2)
		{
			l6 = -l6;
			i3 = -i3;
		}

		if (d0 < 0.0D)
		{
			j1 = MathHelper.clamp(j1, 70, world.getActualHeight() - 10);
			k2 = j1;

			for (int j7 = -1; j7 <= 1; ++j7)
			{
				for (int i8 = 1; i8 < 3; ++i8)
				{
					for (int i9 = -1; i9 < 3; ++i9)
					{
						int l9 = i6 + (i8 - 1) * l6 + j7 * i3;
						int j10 = k2 + i9;
						int l10 = k6 + (i8 - 1) * i3 - j7 * l6;
						boolean flag = i9 < 0;
						blockpos$mutableblockpos.setPos(l9, j10, l10);
						world.setBlockState(blockpos$mutableblockpos, flag ? Blocks.SNOW_BLOCK.getDefaultState() : Blocks.AIR.getDefaultState());
					}
				}
			}
		}

		for (int k7 = -1; k7 < 3; ++k7)
		{
			for (int j8 = -1; j8 < 4; ++j8)
			{
				if (k7 == -1 || k7 == 2 || j8 == -1 || j8 == 3)
				{
					blockpos$mutableblockpos.setPos(i6 + k7 * l6, k2 + j8, k6 + k7 * i3);
					world.setBlockState(blockpos$mutableblockpos, Blocks.SNOW_BLOCK.getDefaultState(), 3);
				}
			}
		}

		BlockState blockstate = this.portalBlock.getDefaultState().with(PagamosPortalBlock.AXIS, l6 == 0 ? Direction.Axis.Z : Direction.Axis.X);

		for (int k8 = 0; k8 < 2; ++k8)
		{
			for (int j9 = 0; j9 < 3; ++j9)
			{
				blockpos$mutableblockpos.setPos(i6 + k8 * l6, k2 + j9, k6 + k8 * i3);
				world.setBlockState(blockpos$mutableblockpos, blockstate, 18);
			}
		}

		return true;
	}

	public void tick(long worldTime)
	{
		if (worldTime % 100L == 0L)
		{
			this.removeStaleColumnPositions(worldTime);
			this.removeStalePortalLocations(worldTime);
		}

	}

	private void removeStaleColumnPositions(long lastUpdateTime)
	{
		LongIterator longiterator = this.columnPosMap.values().iterator();
		while (longiterator.hasNext())
		{
			long i = longiterator.nextLong();
			if (i <= lastUpdateTime)
			{
				longiterator.remove();
			}
		}

	}

	private void removeStalePortalLocations(long lastUpdateTime)
	{
		long i = lastUpdateTime - 300L;
		Iterator<Entry<BlockPos, PagamosTeleporter.PortalPosition>> iterator = this.destinationCoordinateCache.entrySet().iterator();

		while (iterator.hasNext())
		{
			Entry<BlockPos, PagamosTeleporter.PortalPosition> entry = iterator.next();
			PagamosTeleporter.PortalPosition portalPos = entry.getValue();
			if (portalPos.lastUpdateTime < i)
			{
				BlockPos columnpos = entry.getKey();
				this.world.getChunkProvider().registerTicket(TicketType.PORTAL, new ChunkPos(portalPos.pos), 3, columnpos);
				iterator.remove();
			}
		}

	}

	private static double getDistance(BlockPos a, BlockPos b)
	{
		if (a == null || b == null)
			return 0;
		else
		{
			double x = b.getX() - a.getX();
			double y = b.getY() - a.getY();
			double z = b.getZ() - a.getZ();
			return Math.sqrt((x * x) + (y * y) + (z * z));
		}
	}

	static class PortalPosition
	{
		public final BlockPos pos;
		public long lastUpdateTime;

		public PortalPosition(BlockPos pos, long lastUpdateTime)
		{
			this.pos = pos;
			this.lastUpdateTime = lastUpdateTime;
		}
	}
}
