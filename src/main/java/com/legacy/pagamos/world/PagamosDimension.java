package com.legacy.pagamos.world;

import com.legacy.pagamos.registry.PagamosBiomes;
import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.client.renderer.Vector3f;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.provider.BiomeProviderType;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.ChunkGeneratorType;
import net.minecraft.world.gen.NetherGenSettings;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class PagamosDimension extends Dimension
{
	private static final Vec3d ICE_FOG_VEC = new Vec3d(0.045D, 0.066D, 0.15D);

	public PagamosDimension(World worldIn, DimensionType typeIn)
	{
		super(worldIn, typeIn, 0.1F);

		this.nether = true;
	}

	@Override
	public ChunkGenerator<?> createChunkGenerator()
	{
		NetherGenSettings nethergensettings = ChunkGeneratorType.CAVES.createSettings();
		nethergensettings.setDefaultBlock(PagamosBlocks.packed_snow.getDefaultState());
		nethergensettings.setDefaultFluid(PagamosBlocks.crystal_ice.getDefaultState());
		return ChunkGeneratorType.CAVES.create(this.world, BiomeProviderType.FIXED.create(BiomeProviderType.FIXED.createSettings(this.world.getWorldInfo()).setBiome(PagamosBiomes.FROZEN_HELL)), nethergensettings);
	}

	@Override
	public void getLightmapColors(float partialTicks, float sunBrightness, float skyLight, float blockLight, Vector3f colors)
	{
		float light = -0.02F;
		float x = light + 0.07F;
		float y = light + 0.09F;
		float z = light + 0.3F;

		colors.add(new Vector3f(x, y, z));
	}

	@Override
	public boolean isSurfaceWorld()
	{
		return false;
	}

	@Override
	public boolean canRespawnHere()
	{
		return false;
	}

	@Override
	public float calculateCelestialAngle(long worldTime, float partialTicks)
	{
		return 0.0F;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public Vec3d getFogColor(float f, float f1)
	{
		return ICE_FOG_VEC;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public float[] calcSunriseSunsetColors(float celestialAngle, float partialTicks)
	{
		return null;
	}

	@Override
	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid)
	{
		return null;
	}

	@Override
	public BlockPos findSpawn(int posX, int posZ, boolean checkValid)
	{
		return null;
	}

	@Override
	public boolean doesXZShowFog(int x, int z)
	{
		return false;
	}
}
