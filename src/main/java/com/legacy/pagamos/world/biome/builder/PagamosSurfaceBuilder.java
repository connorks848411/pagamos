package com.legacy.pagamos.world.biome.builder;

import java.util.Random;
import java.util.function.Function;

import com.legacy.pagamos.registry.PagamosBlocks;
import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.OctavesNoiseGenerator;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class PagamosSurfaceBuilder extends SurfaceBuilder<SurfaceBuilderConfig>
{
	private static final BlockState CAVE_AIR = Blocks.CAVE_AIR.getDefaultState();
	private static final BlockState PACKED_SNOW = PagamosBlocks.packed_snow.getDefaultState();
	private static final BlockState CRYSTAL_ICE = PagamosBlocks.crystal_ice.getDefaultState();
	private static final BlockState ICY_COBBLESTONE = PagamosBlocks.ice_cobblestone.getDefaultState();
	private static final BlockState CRYSTAL_STONE = PagamosBlocks.crystal_stone.getDefaultState();
	private static final BlockState MARBLE = PagamosBlocks.marble.getDefaultState();
	private static final BlockState SNOW = Blocks.SNOW_BLOCK.getDefaultState();

	protected long field_205552_a;
	protected OctavesNoiseGenerator noise;

	public PagamosSurfaceBuilder(Function<Dynamic<?>, ? extends SurfaceBuilderConfig> config)
	{
		super(config);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void buildSurface(Random random, IChunk chunkIn, Biome biomeIn, int x, int z, int startHeight, double noise, BlockState defaultBlock, BlockState defaultFluid, int seaLevel, long seed, SurfaceBuilderConfig config)
	{
		int i = seaLevel + 1;
		int j = x & 15;
		int k = z & 15;
		boolean flag = this.noise.func_205563_a((double) x * 0.03125D, (double) z * 0.03125D, 0.0D) * 75.0D + random.nextDouble() > 0.0D;
		boolean flag1 = this.noise.func_205563_a((double) x * 0.03125D, 109.0D, (double) z * 0.03125D) * 75.0D + random.nextDouble() > 0.0D;
		int l = (int) (noise / 3.0D + 3.0D + random.nextDouble() * 0.25D);
		BlockPos.Mutable blockpos$mutable = new BlockPos.Mutable();
		int i1 = -1;
		BlockState blockstate = ICY_COBBLESTONE;
		BlockState blockstate1 = ICY_COBBLESTONE;

		for (int j1 = 127; j1 >= 0; --j1)
		{
			blockpos$mutable.setPos(j, j1, k);
			BlockState blockstate2 = chunkIn.getBlockState(blockpos$mutable);
			if (blockstate2.getBlock() != null && !blockstate2.isAir())
			{
				if (blockstate2.getBlock() == defaultBlock.getBlock())
				{
					if (i1 == -1)
					{
						if (l <= 0)
						{
							blockstate = CAVE_AIR;
							blockstate1 = CRYSTAL_STONE;
						}
						else if (j1 > 80 && j1 < 110)
						{
							blockstate = CRYSTAL_ICE;
							blockstate1 = ICY_COBBLESTONE;
						}
						else if (j1 >= i - 4 && j1 <= i + 1)
						{
							blockstate = CRYSTAL_STONE;
							blockstate1 = CRYSTAL_STONE;

							if (flag1)
							{
								blockstate = MARBLE;
								blockstate1 = SNOW;
							}

							if (flag)
							{
								blockstate = ICY_COBBLESTONE;
								blockstate1 = ICY_COBBLESTONE;
							}
						}

						if (j1 < i && (blockstate == null || blockstate.isAir()))
						{
							blockstate = defaultFluid;
						}

						i1 = l;
						if (j1 >= i - 1)
						{
							chunkIn.setBlockState(blockpos$mutable, blockstate, false);
						}
						else
						{
							chunkIn.setBlockState(blockpos$mutable, blockstate1, false);
						}
					}
					else if (i1 > 0)
					{
						--i1;
						chunkIn.setBlockState(blockpos$mutable, blockstate1, false);
					}
				}
			}
			else
			{
				i1 = -1;
			}
		}

	}

	public void setSeed(long seed)
	{
		if (this.field_205552_a != seed || this.noise == null)
		{
			this.noise = new OctavesNoiseGenerator(new SharedSeedRandom(seed), 3, 0);
		}

		this.field_205552_a = seed;
	}
}