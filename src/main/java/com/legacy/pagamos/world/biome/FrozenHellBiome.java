package com.legacy.pagamos.world.biome;

import com.google.common.collect.ImmutableSet;
import com.legacy.pagamos.entity.PagamosEntityTypes;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosFeatures;
import com.legacy.pagamos.world.biome.builder.PagamosSurfaceBuilder;

import net.minecraft.block.Blocks;
import net.minecraft.block.pattern.BlockMatcher;
import net.minecraft.entity.EntityClassification;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.BlockClusterFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig.FillerBlockType;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.FrequencyConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class FrozenHellBiome extends Biome
{
	public static final SurfaceBuilder<SurfaceBuilderConfig> FROZEN_HELL_SURFACE_BUILDER = new PagamosSurfaceBuilder(SurfaceBuilderConfig::deserialize);
	public static final SurfaceBuilderConfig SNOW_CONFIG = new SurfaceBuilderConfig(Blocks.SNOW_BLOCK.getDefaultState(), Blocks.SNOW_BLOCK.getDefaultState(), Blocks.SNOW_BLOCK.getDefaultState());

	public static final BlockClusterFeatureConfig BLUE_FIRE_PATCH_CONFIG = (new BlockClusterFeatureConfig.Builder(new SimpleBlockStateProvider(PagamosBlocks.blue_fire.getDefaultState()), new SimpleBlockPlacer())).tries(64).whitelist(ImmutableSet.of(PagamosBlocks.ice_cobblestone.getBlock())).func_227317_b_().build();

	private static final FillerBlockType PACKED_SNOW_CONFIG = OreFeatureConfig.FillerBlockType.create("packed_snow", "packed_snow", new BlockMatcher(PagamosBlocks.packed_snow));
	private static final FillerBlockType ICY_COBBLE_CONFIG = OreFeatureConfig.FillerBlockType.create("icy_cobblestone", "icy_cobblestone", new BlockMatcher(PagamosBlocks.ice_cobblestone));

	public FrozenHellBiome()
	{
		super((new Biome.Builder()).surfaceBuilder(FROZEN_HELL_SURFACE_BUILDER, SNOW_CONFIG).precipitation(Biome.RainType.NONE).category(Biome.Category.ICY).depth(0.1F).scale(0.2F).temperature(-2.0F).downfall(0.0F).waterColor(4159204).waterFogColor(329011).parent((String) null));

		this.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, Feature.RANDOM_PATCH.withConfiguration(BLUE_FIRE_PATCH_CONFIG).withPlacement(Placement.HELL_FIRE.configure(new FrequencyConfig(10))));
		this.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.CRYSTAL_BLOB.withConfiguration(IFeatureConfig.NO_FEATURE_CONFIG).withPlacement(Placement.LIGHT_GEM_CHANCE.configure(new FrequencyConfig(10))));
		this.addFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.CRYSTAL_BLOB.withConfiguration(IFeatureConfig.NO_FEATURE_CONFIG).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(10, 0, 0, 128))));

		this.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(PACKED_SNOW_CONFIG, PagamosBlocks.frozen_amethyst_ore.getDefaultState(), 8)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(20, 0, 0, 64))));
		this.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(PACKED_SNOW_CONFIG, PagamosBlocks.frozen_sapphire_ore.getDefaultState(), 8)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(4, 0, 0, 32))));

		this.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(ICY_COBBLE_CONFIG, PagamosBlocks.amethyst_ore.getDefaultState(), 8)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(20, 0, 0, 64))));
		this.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, Feature.ORE.withConfiguration(new OreFeatureConfig(ICY_COBBLE_CONFIG, PagamosBlocks.sapphire_ore.getDefaultState(), 8)).withPlacement(Placement.COUNT_RANGE.configure(new CountRangeConfig(4, 0, 0, 32))));

		this.addSpawn(EntityClassification.CREATURE, new SpawnListEntry(PagamosEntityTypes.YETI, 1, 0, 1));
		this.addSpawn(EntityClassification.MONSTER, new SpawnListEntry(PagamosEntityTypes.PHOENIX, 1, 0, 1));
		this.addSpawn(EntityClassification.MONSTER, new SpawnListEntry(PagamosEntityTypes.ICE_CREEPER, 1, 0, 1));
	}
}